import controller.Authenticator;
import controller.Bibilioteca;
import controller.MenuList;
import model.Library;
import model.ListLoader;
import model.libraryitems.ItemList;
import model.libraryitems.book.BookParser;
import model.libraryitems.movie.MovieParser;
import model.user.User;
import model.user.UserParser;
import view.ConsoleInput;
import view.ConsoleOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class BibiliotecaApp {
    public static final String BOOK_FILE_NAME = "res/Books";
    public static final String MOVIES_FILE_NAME = "res/Movies";
    public static final String USERS_FILE = "res/Users";

    public static void main(String[] args) throws IOException {
        ConsoleOutput consoleOutput = new ConsoleOutput();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        ConsoleInput consoleInput = new ConsoleInput(consoleOutput, bufferedReader);


        ListLoader bookListLoader = new ListLoader(new BufferedReader(new FileReader(BOOK_FILE_NAME)), new BookParser());
        ItemList bookList = bookListLoader.buildLibraryItemList();

        ListLoader movieListLoader = new ListLoader(new BufferedReader(new FileReader(MOVIES_FILE_NAME)), new MovieParser());
        ItemList movieList = movieListLoader.buildLibraryItemList();

        ListLoader userListLoader = new ListLoader(new BufferedReader(new FileReader(USERS_FILE)), new UserParser());
        List<User> userList = userListLoader.buildUserList();

        Library library = new Library(bookList, movieList, userList);

        Authenticator authenticator = new Authenticator(userList);

        Bibilioteca bibilioteca = new Bibilioteca(library, consoleOutput, consoleInput, new MenuList(), authenticator);
        bibilioteca.start();
    }
}
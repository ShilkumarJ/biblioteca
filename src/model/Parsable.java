package model;

public interface Parsable {
    LibraryItem parse(String item) throws InvalidInputFormatException;
}

package model.libraryitems;

import java.util.LinkedList;

public class ItemList extends LinkedList<Storable> {
    public Storable getItemByName(String bookName) {
        if (!isValidItemName(bookName)) {
            return null;
        }
        for (Storable libraryItem : this) {
            if (libraryItem.hasName(bookName)) {
                return libraryItem;
            }
        }
        return null;
    }

    private boolean isValidItemName(String itemName) {
        if (itemName == null || itemName.equals("")) {
            return false;
        }
        return true;
    }

    public ItemList search(String itemName) {
        if (!isValidItemName(itemName)) {
            return null;
        }
        ItemList itemList = new ItemList();
        for (Storable libraryItem : this) {
            if (libraryItem.hasSubString(itemName)) {
                itemList.add(libraryItem);
            }
        }
        return itemList;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("List:\n");
        for (Storable libraryItem :
                this) {
            string.append(libraryItem.toString());
            string.append("\n");
        }
        return string.toString();
    }
}

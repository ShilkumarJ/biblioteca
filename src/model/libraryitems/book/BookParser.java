package model.libraryitems.book;

import model.InvalidInputFormatException;
import model.LibraryItem;
import model.Parsable;

public class BookParser implements Parsable {
    public LibraryItem parse(String input) throws InvalidInputFormatException {
        String[] attributes = input.split(",");
        try {
            String bookName = attributes[0];
            String authorName = attributes[1];
            int yearOfPublication = Integer.parseInt(attributes[2].trim());
            return new Book(bookName, authorName, yearOfPublication);
        } catch (Exception e) {
            throw new InvalidInputFormatException();
        }
    }
}

package model.libraryitems.book;

import model.LibraryItem;
import model.libraryitems.Storable;

//understands entity in the library
public class Book implements Storable, LibraryItem {
    private final String name;
    private final String author;
    private final int yearOfPublication;

    public Book(String name, String author, int yearOfPublication) {
        this.name = name;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
    }

    @Override
    public String toString() {
        return String.format("%-30s%-20s%s", name, author, yearOfPublication);
    }

    public boolean hasName(String itemName) {
        return name.equalsIgnoreCase(itemName);
    }

    @Override
    public boolean hasSubString(String itemName) {
        return name.toLowerCase().contains(itemName.toLowerCase());
    }
}

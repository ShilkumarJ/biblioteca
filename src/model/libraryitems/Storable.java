package model.libraryitems;

public interface Storable {
    boolean hasName(String itemName);

    boolean hasSubString(String itemName);
}

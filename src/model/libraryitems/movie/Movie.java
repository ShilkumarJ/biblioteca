package model.libraryitems.movie;

import model.LibraryItem;
import model.libraryitems.Storable;

public class Movie implements Storable, LibraryItem {
    private String name;
    private int year;
    private String director;
    private String movieRating;


    public Movie(String name, int year, String director, String movieRating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.movieRating = movieRating;
    }

    @Override
    public String toString() {
        return String.format("%-30s%-20s%-20s%-20s", name, year, director, movieRating);
    }

    @Override
    public boolean hasName(String itemName) {
        return name.equalsIgnoreCase(itemName);
    }

    @Override
    public boolean hasSubString(String itemName) {
        return name.toLowerCase().contains(itemName.toLowerCase());
    }
}

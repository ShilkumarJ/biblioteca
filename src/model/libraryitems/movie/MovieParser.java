package model.libraryitems.movie;

import model.InvalidInputFormatException;
import model.Parsable;

public class MovieParser implements Parsable {

    public Movie parse(String input) throws InvalidInputFormatException {
        String[] attributes = input.split(",");
        try {
            String movieName = attributes[0].trim();
            int year = Integer.parseInt(attributes[1].trim());
            String director = attributes[2].trim();
            String rating = attributes[3].trim();
            return new Movie(movieName, year, director, rating);
        } catch (Exception e) {
            throw new InvalidInputFormatException();
        }
    }
}

package model.user;

import model.InvalidInputFormatException;
import model.LibraryItem;
import model.Parsable;

public class UserParser implements Parsable {

    public LibraryItem parse(String item) throws InvalidInputFormatException {
        String[] attributes = item.split(",");
        try {
            String name = attributes[0].trim();
            String emailId = attributes[1].trim();
            String phoneNo = attributes[2].trim();
            String libraryNumber = attributes[3].trim();
            String password = attributes[4].trim();
            return new User(name, emailId, phoneNo, libraryNumber, password);
        } catch (Exception e) {
            throw new InvalidInputFormatException();
        }
    }
}

package model.user;

import model.LibraryItem;

public class User implements LibraryItem {

    private String name;
    private String emailId;
    private String phoneNo;
    private String libraryNumber;
    private String password;

    public User(String name, String emailId, String phoneNo, String libraryNumber, String password) {
        this.name = name;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.libraryNumber = libraryNumber;
        this.password = password;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder(" libraryNumber : " + libraryNumber + '\n');
        string.append(" Name   \t: " + name + '\n');
        string.append(" EmailId\t: " + emailId + '\n');
        string.append(" phoneNo\t: " + phoneNo + '\n');
        return string.toString();
    }

    public boolean hasLibraryNumber(String libraryNumber) {
        return this.libraryNumber.equals(libraryNumber);
    }

    public boolean hasPassword(String password) {
        return this.password.equals(password);
    }
}

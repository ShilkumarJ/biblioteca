package model;

import controller.ItemIsNotAvailableException;
import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import model.libraryitems.movie.Movie;
import model.user.User;

import java.util.HashMap;
import java.util.List;

//understands collection of books and movies
public class Library {
    public static final User defaultUser = new User("", "", "", "", "");
    private ItemList bookList;
    private ItemList movieList;
    private ItemList checkedOutBooks;
    private List<User> users;
    private HashMap<Book, User> register;

    public Library(ItemList bookList, ItemList movieList, List<User> userList) {
        this.bookList = bookList;
        this.movieList = movieList;
        this.checkedOutBooks = new ItemList();
        this.users = userList;
        this.register = new HashMap<>();
    }

    public Book checkoutBook(String bookName, User user) throws ItemIsNotAvailableException {
        Book book = (Book) bookList.getItemByName(bookName);
        if (book == null) {
            if (checkedOutBooks.getItemByName(bookName) == null) {
                throw ItemIsNotAvailableException.invalidBookName();
            }
            throw ItemIsNotAvailableException.bookIsAlreadyCheckedOut();
        }
        bookList.remove(book);
        checkedOutBooks.add(book);
        register.put(book, user);
        return book;
    }

    public boolean checkInBook(String bookName, User currentUser) throws ItemIsNotAvailableException {
        Book book = (Book) checkedOutBooks.getItemByName(bookName);
        if (register.remove(book) != currentUser) {
            throw ItemIsNotAvailableException.invalidBookToCheckIn();
        }
        bookList.add(book);
        checkedOutBooks.remove(book);
        return true;
    }

    public ItemList search(String bookName) {
        return bookList.search(bookName);
    }

    public ItemList bookList() {
        return bookList;
    }

    public ItemList movieList() {
        return movieList;
    }

    public Movie checkoutMovie(String movieName) throws ItemIsNotAvailableException {
        Movie movie = (Movie) movieList.getItemByName(movieName);
        if (movie == null) {
            throw ItemIsNotAvailableException.invalidMovieName();
        }
        movieList.remove(movie);
        return movie;
    }
}

package model;

import model.libraryitems.ItemList;
import model.libraryitems.Storable;
import model.user.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListLoader {

    private BufferedReader bufferedReader;
    private Parsable parsable;

    public ListLoader(BufferedReader bufferedReader, Parsable parsable) {
        this.bufferedReader = bufferedReader;
        this.parsable = parsable;
    }

    public List<User> buildUserList() throws IOException {
        List<User> list = new ArrayList<>();
        String user;
        while ((user = bufferedReader.readLine()) != null) {
            try {
                list.add((User) parsable.parse(user));
            } catch (InvalidInputFormatException e) {
            }
        }
        return list;
    }

    public ItemList buildLibraryItemList() throws IOException {
        ItemList list = new ItemList();
        String libraryItem;
        while ((libraryItem = bufferedReader.readLine()) != null) {
            try {
                list.add((Storable) parsable.parse(libraryItem));
            } catch (InvalidInputFormatException e) {
            }
        }
        return list;
    }
}
package controller;

public enum Options {
    QUIT(0), SHOW_LIST_OF_BOOKS(1), SEARCH(2), CHECKOUT_BOOK(3), RETURN_BOOK(4), SHOW_LIST_OF_MOVIES(5), CHECKOUT_MOVIE(6),
    USER_INFO(7), INVALID(8);
    int value;

    Options(int value) {
        this.value = value;
    }

    public Options get(int value) {
        for (Options options :
                Options.values()) {
            if (options.value == value) {
                return options;
            }
        }
        return INVALID;
    }
}


package controller;

import model.Library;
import model.user.User;

import java.util.List;

public class Authenticator {
    private List<User> userList;

    public Authenticator(List<User> userList) {
        this.userList = userList;
    }

    User authenticate(String libraryNumber, String password) {
        if (!validateLibraryNumber(libraryNumber) || !validatePassword(password)) {
            return Library.defaultUser;
        }

        for (User user :
                userList) {
            if (user.hasLibraryNumber(libraryNumber)) {
                if (user.hasPassword(password)) {
                    return user;
                }
            }
        }
        return Library.defaultUser;
    }

    private boolean validateLibraryNumber(String libraryNumber) {
        return libraryNumber.matches("[0-9]{3}[-][0-9]{4}");
    }

    private boolean validatePassword(String password) {
        return password.length() > 5;
    }
}
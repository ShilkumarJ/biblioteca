package controller;

public class UserMessages {
    public static final String BOOK_IS_ALREADY_CHECKED_OUT = "Book is already checked out";
    public static final String INVALID_OPTION_MESSAGE = "Select a valid option!";
    public static final String WELCOME_MESSAGE = "Welcome To Library";
    public static final String CLOSING_APPLICATION_MESSAGE = "Application is closing";
    public static final String SUCCESSFUL_BOOK_CHECKOUT_MESSAGE = "Thank you! Enjoy the Book";
    public static final String SUCCESSFUL_MOVIE_CHECKOUT_MESSAGE = "Thank you! Enjoy the Movie";
    public static final String BOOK_IS_NOT_AVAILABLE = "Book is not available in the Library";
    public static final String SUCCESSFUL_BOOK_RETURN_MESSAGE = "Thank you for returning the book";
    public static final String UNSUCCESSFUL_BOOK_RETURN_MESSAGE = "That is not a valid book to return.";
    public static final String INVALID_MOVIE_NAME_MESSAGE = "Invalid Movie Name";
    public static final String SUCCESSFUL_LOGIN_MESSAGE = "Login Successful";
    public static final String INVALID_LOGIN_CREDENTIALS_MESSAGE = "Invalid Login credentials";
    public static final String ASK_FOR_LIBRARY_NUMBER = "Enter Library Number:";
    public static final String ENTER_PASSWORD_MESSAGE = "Enter Password";

}
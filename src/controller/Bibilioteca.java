package controller;

import model.Library;
import model.libraryitems.ItemList;
import model.libraryitems.Storable;
import model.libraryitems.book.Book;
import model.user.User;
import view.ConsoleInput;
import view.ConsoleOutput;

import static controller.Options.QUIT;
import static controller.UserMessages.*;

//understands how to manage Library
public class Bibilioteca {

    private final Authenticator authenticator;
    private ConsoleOutput consoleOutput;
    private ConsoleInput consoleInput;
    private boolean run;
    private MenuList menuList;
    private Library library;
    private User currentUser = Library.defaultUser;

    public Bibilioteca(Library library, ConsoleOutput consoleOutput, ConsoleInput consoleInput, MenuList menuList, Authenticator authenticator) {
        this.consoleOutput = consoleOutput;
        this.consoleInput = consoleInput;
        this.menuList = menuList;
        this.library = library;
        this.authenticator = authenticator;
    }

    public void start() {
        consoleOutput.showMessage(WELCOME_MESSAGE);
        while (!login()) ;

        consoleOutput.showMessage(SUCCESSFUL_LOGIN_MESSAGE);
        run = true;
        showMenu(menuList);
    }

    private boolean login() {
        consoleOutput.showMessage(UserMessages.ASK_FOR_LIBRARY_NUMBER);
        String libraryNumber = consoleInput.getString();

        consoleOutput.showMessage(ENTER_PASSWORD_MESSAGE);
        String password = consoleInput.getString();

        User user = authenticator.authenticate(libraryNumber, password);
        if (user == Library.defaultUser) {
            consoleOutput.showErrorMessage(INVALID_LOGIN_CREDENTIALS_MESSAGE);
            return false;
        }
        currentUser = user;
        return true;
    }

    private void showMenu(MenuList menuList) {
        Options options = QUIT;
        while (run) {
            consoleOutput.showMessage(menuList.toString());
            int optionNumber = consoleInput.getInteger();
            doOperation(options.get(optionNumber));
        }
    }

    private void doOperation(Options options) {
        switch (options) {
            case QUIT:
                consoleOutput.showMessage(CLOSING_APPLICATION_MESSAGE);
                run = false;
                break;

            case SHOW_LIST_OF_BOOKS:
                showListOfBooks();
                break;

            case SEARCH:
                search();
                break;

            case CHECKOUT_BOOK:
                checkoutBook();
                break;

            case RETURN_BOOK:
                checkInBook();
                break;

            case SHOW_LIST_OF_MOVIES:
                showListOfMovies();
                break;
            case CHECKOUT_MOVIE:
                checkoutMovie();
                break;
            case USER_INFO:
                shoUserInfo();
            default:
                consoleOutput.showErrorMessage(INVALID_OPTION_MESSAGE);
        }
    }

    private void shoUserInfo() {
        consoleOutput.showMessage(currentUser.toString());
    }

    private void checkoutMovie() {
        consoleOutput.showMessage("Enter Name:");
        String movieName = consoleInput.getString();
        try {
            library.checkoutMovie(movieName);
            consoleOutput.showMessage(SUCCESSFUL_MOVIE_CHECKOUT_MESSAGE);
        } catch (ItemIsNotAvailableException e) {
            consoleOutput.showMessage(INVALID_MOVIE_NAME_MESSAGE);
        }
    }

    private void showListOfMovies() {
        ItemList movieList = library.movieList();
        consoleOutput.showMessage(movieList.toString());
    }

    private void showListOfBooks() {
        ItemList itemList = library.bookList();
        consoleOutput.showMessage(itemList.toString());
    }

    private void search() {
        String bookName = consoleInput.getString();
        ItemList books = library.search(bookName);
        consoleOutput.showMessage(books.toString());
    }

    private boolean checkInBook() {
        consoleOutput.showMessage("Enter Name:");
        String bookName = consoleInput.getString();
        try {
            library.checkInBook(bookName, currentUser);
            consoleOutput.showMessage(SUCCESSFUL_BOOK_RETURN_MESSAGE);
        } catch (ItemIsNotAvailableException e) {
            consoleOutput.showErrorMessage(e.getMessage());
            return false;
        }
        return true;
    }

    private Book checkoutBook() {
        consoleOutput.showMessage("Enter Name:");
        String bookName = consoleInput.getString();
        Storable book = null;
        try {
            book = library.checkoutBook(bookName, currentUser);
            consoleOutput.showMessage(SUCCESSFUL_BOOK_CHECKOUT_MESSAGE);
        } catch (ItemIsNotAvailableException e) {
            consoleOutput.showErrorMessage(e.getMessage());
        }
        return (Book) book;
    }
}

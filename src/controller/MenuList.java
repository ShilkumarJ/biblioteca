package controller;

import java.util.ArrayList;

//Understands list of option provided by application
public class MenuList extends ArrayList<String> {
    public MenuList() {
        add("Quit");
        add("List Books");
        add("Search Book");
        add("Checkout Book");
        add("Return Book");
        add("List Movies");
        add("Checkout Movie");
        add("Show User Info");
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("Menu:\n");
        int number = 0;
        for (String menuOption :
                this) {
            string.append(number);
            string.append(" : ");
            string.append(menuOption);
            string.append("\n");
            number++;
        }
        return string.toString();
    }
}

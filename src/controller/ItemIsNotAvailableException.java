package controller;

import static controller.UserMessages.*;

public class ItemIsNotAvailableException extends Throwable {

    String message;

    private ItemIsNotAvailableException(String message) {
        super(message);
    }

    public static ItemIsNotAvailableException invalidBookName() {
        return new ItemIsNotAvailableException(BOOK_IS_NOT_AVAILABLE);
    }

    public static ItemIsNotAvailableException bookIsAlreadyCheckedOut() {
        return new ItemIsNotAvailableException(BOOK_IS_ALREADY_CHECKED_OUT);
    }

    public static ItemIsNotAvailableException invalidBookToCheckIn() {
        return new ItemIsNotAvailableException(UNSUCCESSFUL_BOOK_RETURN_MESSAGE);
    }

    public static ItemIsNotAvailableException invalidMovieName() {
        return new ItemIsNotAvailableException(INVALID_MOVIE_NAME_MESSAGE);
    }
}

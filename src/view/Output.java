package view;

public interface Output {
    void showErrorMessage(String message);

    void showMessage(String message);
}

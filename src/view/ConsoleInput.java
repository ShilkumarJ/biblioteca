package view;

import java.io.BufferedReader;
import java.io.IOException;

public class ConsoleInput implements Input {

    private final BufferedReader bufferedReader;
    private final Output output;

    public ConsoleInput(Output output, BufferedReader bufferedReader) {
        this.output = output;
        this.bufferedReader = bufferedReader;
    }

    public int getInteger() {
        int option = -1;
        output.showMessage("Enter Option:");
        try {
            option = Integer.parseInt(bufferedReader.readLine());
        } catch (Exception e) {
        }
        return option;
    }

    public String getString() {
        String bookName = null;
        try {
            bookName = bufferedReader.readLine();
        } catch (IOException e) {
        }
        return bookName;
    }
}

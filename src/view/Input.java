package view;

public interface Input {
    int getInteger();

    String getString();
}

package view;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConsoleInputTest {
    private ConsoleInput consoleInput;
    private BufferedReader bufferedReader;
    private ConsoleOutput consoleOutput;


    @Before
    public void setUp() throws Exception {
        bufferedReader = mock(BufferedReader.class);
        consoleOutput = mock(ConsoleOutput.class);
        consoleInput = new ConsoleInput(consoleOutput, bufferedReader);
    }

    @Test
    public void getOptionShouldReturnOptionEnteredByUser() throws Exception {
        when(bufferedReader.readLine()).thenReturn("2");
        assertEquals(2, consoleInput.getInteger());
    }

    @Test
    public void getBookNameShouldReturnBookNameEnteredByUser() throws Exception {
        when(bufferedReader.readLine()).thenReturn("java");
        assertEquals("java", consoleInput.getString());
    }
}

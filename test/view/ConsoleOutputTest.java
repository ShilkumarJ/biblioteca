package view;

import controller.UserMessages;
import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ConsoleOutputTest {
    private ByteArrayOutputStream byteArrayOutputStream;

    @Before
    public void setUp() throws Exception {
        byteArrayOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @Test
    public void shouldShowWelcomeMessageWhenApplicationStarts() throws Exception {
        ConsoleOutput consoleOutput = new ConsoleOutput();
        consoleOutput.showMessage(UserMessages.WELCOME_MESSAGE);
        String expected = UserMessages.WELCOME_MESSAGE + "\n";
        assertEquals(expected, byteArrayOutputStream.toString());
    }

    @Test
    public void shouldPrintErrorMessageForGivenErrorMessage() throws Exception {
        ConsoleOutput consoleOutput = new ConsoleOutput();
        consoleOutput.showErrorMessage(UserMessages.UNSUCCESSFUL_BOOK_RETURN_MESSAGE);
        String expected = UserMessages.UNSUCCESSFUL_BOOK_RETURN_MESSAGE + "\n";
        assertEquals(expected, byteArrayOutputStream.toString());
    }

    @Test
    public void shouldShowListOfBooksAfterWelcomeMessage() throws Exception {
        ConsoleOutput consoleOutput = new ConsoleOutput();

        ItemList itemList = getList();
        consoleOutput.showMessage(itemList.toString());


        String expected = expectedOutput();
        assertEquals(expected, byteArrayOutputStream.toString());
    }

    private String expectedOutput() {
        String expected = "List:";
        expected += "\n" + String.format("%-30s%-20s%s", "Headfirst Java", "Mahadev", 2000);
        expected += "\n" + String.format("%-30s%-20s%s", "Database", "Swapnil", 2001);
        expected += "\n" + String.format("%-30s%-20s%s", "Artificial Intelligence", "D Khaperkhuntikar", 2002);
        expected += "\n" + String.format("%-30s%-20s%s", "Operating System", "Gopinath Langote", 1997);
        expected += "\n" + String.format("%-30s%-20s%s", "Computer Network", "Prjakta Desai", 2002) + "\n\n";
        return expected;
    }

    private ItemList getList() {
        ItemList books = new ItemList();
        books.add(new Book("Headfirst Java", "Mahadev", 2000));
        books.add(new Book("Database", "Swapnil", 2001));
        books.add(new Book("Artificial Intelligence", "D Khaperkhuntikar", 2002));
        books.add(new Book("Operating System", "Gopinath Langote", 1997));
        books.add(new Book("Computer Network", "Prjakta Desai", 2002));
        return books;
    }
}

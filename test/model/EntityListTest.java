package model;

import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EntityListTest {

    private Book database;
    private ItemList itemList;
    private Book java;
    private Book database1;


    @Before
    public void setUp() throws Exception {
        itemList = new ItemList();
        database = new Book("Database", "Korth", 1999);
        database1 = new Book("Database1", "Korth", 2000);
        java = new Book("java", "Mahadev", 1997);
        itemList.add(database);
        itemList.add(database1);
        itemList.add(java);
    }

    @Test
    public void getBookByNameShouldReturnBookForGivenBookName() throws Exception {
        assertEquals(database.toString(), itemList.getItemByName("Database").toString());
    }

    @Test
    public void getBookByNameShouldReturnNullIfBookIsNotAvailable() throws Exception {
        assertNull(itemList.getItemByName("abc"));
    }

    @Test
    public void getBookByNameShouldReturnNullForNullInput() throws Exception {
        assertNull(itemList.getItemByName(null));
    }

    @Test
    public void getBookByNameShouldReturnNullForEmptyString() throws Exception {
        assertNull(itemList.getItemByName(""));
    }

    @Test
    public void searchShouldReturnListOfBooksContainingGivenString() throws Exception {
        ItemList expectedList = new ItemList();
        expectedList.add(new Book("Database", "Korth", 1999));
        expectedList.add(new Book("Database1", "Korth", 2000));
        assertEquals(expectedList.toString(), itemList.search("data").toString());
    }

    @Test
    public void searchShouldReturnNullForInvalidString() throws Exception {
        assertNull(itemList.search(null));
    }
}

package model.user;

import model.InvalidInputFormatException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserParserTest {
    private UserParser userParser;

    @Before
    public void setUp() throws Exception {
        userParser = new UserParser();
    }

    @Test
    public void parseShouldReturnProductForGivenInputString() throws Exception {
        User user = new User("Swapnil", "swapnilg@thoughtworks.com", "8889999777", "000-0001", "password");
        String input = "Swapnil, swapnilg@thoughtworks.com, 8889999777, 000-0001, password";
        assertEquals(user.toString(), userParser.parse(input).toString());
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseShouldThrowAnExceptionIfInputIsInvalid() throws Exception {
        String input = "Swapnil, swapnilg@thoughtworks.com, 8889999777";
        userParser.parse(input);
    }
}

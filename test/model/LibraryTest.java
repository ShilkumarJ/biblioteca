package model;

import controller.ItemIsNotAvailableException;
import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import model.libraryitems.movie.Movie;
import model.user.User;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class LibraryTest {

    private Library library;
    private ItemList itemList;
    private ItemList movieList;
    private List<User> userList;
    private User user;

    @Before
    public void setUp() throws Exception {
        itemList = new ItemList();
        movieList = new ItemList();
        userList = new LinkedList<>();
        user = mock(User.class);
        library = new Library(itemList, movieList, userList);
        itemList.add(new Book("Headfirst Java", "XYZ", 2010));
        itemList.add(new Book("Database", "Swapnil", 2001));
        itemList.add(new Book("Database1", "Swapnil", 2002));
        movieList.add(new Movie("3 Idiots", 2010, "Rajkumar Hirani", "9"));
        movieList.add(new Movie("Kal Ho Na Ho", 2013, "Karan Johar", "9"));
    }

    @Test
    public void searchShouldReturnListOfBooksForGivenString() throws Exception {
        ItemList expectedList = new ItemList();
        expectedList.add(new Book("Database", "Swapnil", 2001));
        expectedList.add(new Book("Database1", "Swapnil", 2002));
        assertEquals(expectedList.toString(), library.search("Data").toString());
    }

    @Test
    public void checkoutBookShouldReturnBookIfBookIsAvailable() throws Exception, ItemIsNotAvailableException {
        ItemList itemList = library.bookList();
        Book java = (Book) itemList.get(0);
        assertEquals(java, library.checkoutBook("Headfirst Java", user));
    }

    @Test(expected = ItemIsNotAvailableException.class)
    public void checkoutBookShouldThrowAnExceptionWhenBookNameIsInvalid() throws Exception, ItemIsNotAvailableException {
        library.checkoutBook("abc", user);
    }

    @Test(expected = ItemIsNotAvailableException.class)
    public void checkoutBookShouldThrowAnExceptionIfBookIsAlreadyCheckedOut() throws Exception, ItemIsNotAvailableException {
        library.checkoutBook("Headfirst Java", user);
        library.checkoutBook("Headfirst Java", user);
    }

    @Test
    public void returnBookShouldReturnTrueIfBookIsSuccessfullyCheckedIn() throws Exception, ItemIsNotAvailableException {
        library.checkoutBook("Headfirst Java", user);
        assertTrue(library.checkInBook("Headfirst Java", user));
    }

    @Test(expected = ItemIsNotAvailableException.class)
    public void returnBookShouldThrowAnExceptionWhenBookIsNotValidToReturn() throws Exception, ItemIsNotAvailableException {
        library.checkInBook("Headfirst Java", user);
    }

    @Test
    public void checkoutMovieShouldReturnMovieIfAvailable() throws Exception, ItemIsNotAvailableException {
        ItemList movieList = library.movieList();
        Movie threeIdiots = (Movie) movieList.get(0);
        assertEquals(threeIdiots, library.checkoutMovie("3 Idiots"));
    }

    @Test(expected = ItemIsNotAvailableException.class)
    public void checkoutMovieShouldThrowAnExceptionIfBookIsNotAvailable() throws Exception, ItemIsNotAvailableException {
        library.checkoutMovie("abcd");
    }
}

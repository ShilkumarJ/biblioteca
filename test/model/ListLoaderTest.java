package model;

import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import model.user.User;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ListLoaderTest {
    private BufferedReader bufferedReader;
    private Parsable parsable;
    private ListLoader listLoader;

    @Before
    public void setUp() throws Exception {
        bufferedReader = mock(BufferedReader.class);
        parsable = mock(Parsable.class);
        listLoader = new ListLoader(bufferedReader, parsable);
    }

    @Test
    public void buildItemListShouldReturnListOfBooks() throws Exception {
        when(parsable.parse("Headfirst Java,Mahadev,2000")).thenReturn(new Book("Headfirst Java", "Mahadev", 2000));
        when(parsable.parse("Database,Swapnil,2001")).thenReturn(new Book("Database", "Swapnil", 2001));
        when(bufferedReader.readLine()).thenReturn("Headfirst Java,Mahadev,2000", "Database,Swapnil,2001", null);
        ItemList expectedList = new ItemList();
        ItemList actualList = listLoader.buildLibraryItemList();
        expectedList.add(new Book("Headfirst Java", "Mahadev", 2000));
        expectedList.add(new Book("Database", "Swapnil", 2001));
        assertEquals(expectedList.toString(), actualList.toString());
    }

    @Test
    public void buildUserListShouldReturnListOfUsers() throws Exception {
        User user = new User("Swapnil", "swapnilg@thoughtworks.com", "8889999777", "000-0001", "password");
        when(parsable.parse("Swapnil,swapnilg@thoughtworks.com,8889999777,000-0001,password")).thenReturn(user);
        when(bufferedReader.readLine()).thenReturn("Swapnil,swapnilg@thoughtworks.com,8889999777,000-0001,password",null);
        List<User> expectedList = new LinkedList<>();
        expectedList.add(user);
        assertEquals(expectedList, listLoader.buildUserList());
    }

}

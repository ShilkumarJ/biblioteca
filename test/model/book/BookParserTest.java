package model.book;

import model.InvalidInputFormatException;
import model.libraryitems.book.Book;
import model.libraryitems.book.BookParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookParserTest {
    @Test
    public void parseShouldReturnProductForGivenInputString() throws Exception {
        BookParser bookParser = new BookParser();
        Book book = new Book("Headfirst Java", "XYZ", 2010);
        String input = "Headfirst Java,XYZ,2010";
        assertEquals(book.toString(), bookParser.parse(input).toString());
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseShouldThrowAnExceptionIfInputIsInvalid() throws Exception {
        BookParser bookParser = new BookParser();
        String input = "Headfirst Java,XYZ";
        bookParser.parse(input);
    }
}

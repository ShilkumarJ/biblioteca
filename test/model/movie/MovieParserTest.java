package model.movie;

import model.InvalidInputFormatException;
import model.libraryitems.book.BookParser;
import model.libraryitems.movie.Movie;
import model.libraryitems.movie.MovieParser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MovieParserTest {
    private MovieParser movieParser;

    @Before
    public void setUp() throws Exception {
        movieParser = new MovieParser();

    }

    @Test
    public void parseShouldReturnMovieObject() throws Exception {
        String input = "3 Idiots, 2010, Rajkumar Hirani,9";
        Movie expectedMovie = new Movie("3 Idiots", 2010, "Rajkumar Hirani", "9");
        assertEquals(expectedMovie.toString(), movieParser.parse(input).toString());
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseShouldThrowAnExceptionIfInputIsInvalid() throws Exception {
        BookParser bookParser = new BookParser();
        String input = "3 Idiots, 2010, Rajkumar Hirani";
        bookParser.parse(input);
    }

}

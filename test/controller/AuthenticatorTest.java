package controller;

import model.Library;
import model.user.User;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AuthenticatorTest {
    private Authenticator authenticator;
    private User user;


    @Before
    public void setUp() throws Exception {
        List<User> users = new LinkedList<>();
        user = new User("Swapnil", "swapnilg@thoughtworks.com", "8889999777", "000-0001", "password");
        users.add(user);
        authenticator = new Authenticator(users);
    }

    @Test
    public void shouldAuthenticateUser() throws Exception {
        assertEquals(user, authenticator.authenticate("000-0001", "password"));
    }

    @Test
    public void authenticateShouldValidateLibraryNumber() throws Exception {
        assertEquals(Library.defaultUser, authenticator.authenticate("000001", "password"));
    }

    @Test
    public void authenticateShouldValidatePassword() throws Exception {
        assertEquals(Library.defaultUser, authenticator.authenticate("000-0001", "pass"));
    }
}

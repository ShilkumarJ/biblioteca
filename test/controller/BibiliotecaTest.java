package controller;

import model.Library;
import model.libraryitems.ItemList;
import model.libraryitems.book.Book;
import model.libraryitems.movie.Movie;
import model.user.User;
import org.junit.Before;
import org.junit.Test;
import view.ConsoleInput;
import view.ConsoleOutput;

import static controller.UserMessages.SUCCESSFUL_MOVIE_CHECKOUT_MESSAGE;
import static controller.UserMessages.WELCOME_MESSAGE;
import static org.mockito.Mockito.*;

public class BibiliotecaTest {

    private Bibilioteca bibilioteca;
    private ConsoleOutput consoleOutput;
    private ItemList itemList;
    private ItemList movieList;
    private ConsoleInput consoleInput;
    private MenuList menuList;
    private Library library;
    private User user;
    private Authenticator authenticator;

    @Before
    public void setUp() throws Exception {
        consoleOutput = mock(ConsoleOutput.class);
        consoleInput = mock(ConsoleInput.class);
        itemList = new ItemList();
        itemList.add(new Book("Headfirst Java", "XYZ", 2010));
        itemList.add(new Book("Database", "Swapnil", 2001));
        movieList = new ItemList();
        movieList.add(new Movie("3 Idiots", 2010, "Rajkumar Hirani", "9"));
        movieList.add(new Movie("Kal Ho Na Ho", 2013, "Karan Johar", "9"));
        menuList = new MenuList();
        user = mock(User.class);
        library = mock(Library.class);
        authenticator = mock(Authenticator.class);
        when(authenticator.authenticate(anyString(), anyString())).thenReturn(user);
        bibilioteca = new Bibilioteca(library, consoleOutput, consoleInput, menuList, authenticator);
    }

    @Test
    public void startShouldCallShowMessageMethod() throws Exception {
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
    }

    @Test
    public void shouldDisplayInvalidOptionMessageForInvalidOption() throws Exception {
        when(consoleInput.getInteger()).thenReturn(-1, 0);
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showErrorMessage(UserMessages.INVALID_OPTION_MESSAGE);
    }

    @Test
    public void shouldQuitApplicationForOption0() throws Exception {
        when(consoleInput.getInteger()).thenReturn(0);
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(UserMessages.CLOSING_APPLICATION_MESSAGE);
    }

    @Test
    public void shouldShowListOfBooksForOption1() throws Exception {
        when(consoleInput.getInteger()).thenReturn(1, 0);
        when(library.bookList()).thenReturn(itemList);
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(menuList.toString());
        verify(consoleOutput, atLeastOnce()).showMessage(itemList.toString());
    }

    @Test
    public void searchShouldReturnListOfBooksForGivenStringForOption2() throws Exception {
        when(consoleInput.getInteger()).thenReturn(2, 0);
        when(consoleInput.getString()).thenReturn("Java");
        ItemList expectedItemList = new ItemList();
        expectedItemList.add(new Book("Headfirst Java", "XYZ", 2010));
        when(library.search("Java")).thenReturn(expectedItemList);
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(menuList.toString());
        verify(consoleOutput, atLeastOnce()).showMessage(expectedItemList.toString());
    }

    @Test
    public void shouldCheckoutBookForOption3() throws ItemIsNotAvailableException {
        when(consoleInput.getInteger()).thenReturn(3, 0);
        String bookName = "Headfirst Java";
        when(consoleInput.getString()).thenReturn(bookName);
        when(library.checkoutBook(bookName, user)).thenReturn(new Book("Headfirst Java", "XYZ", 2010));
        bibilioteca.start();


        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(UserMessages.SUCCESSFUL_BOOK_CHECKOUT_MESSAGE);
    }

    @Test
    public void shouldNotifyIfBookIsAlreadyCheckedOut() throws Exception, ItemIsNotAvailableException {
        String bookName = "Headfirst Java";
        when(consoleInput.getInteger()).thenReturn(3, 0);
        when(consoleInput.getString()).thenReturn(bookName);
        when(library.checkoutBook(bookName, user)).thenThrow(ItemIsNotAvailableException.bookIsAlreadyCheckedOut());
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showErrorMessage(UserMessages.BOOK_IS_ALREADY_CHECKED_OUT);
    }

    @Test
    public void shouldNotifyIfBookIsNotAvailableInLibrary() throws Exception, ItemIsNotAvailableException {
        String bookName = "abcd";
        when(consoleInput.getInteger()).thenReturn(3, 0);
        when(consoleInput.getString()).thenReturn(bookName);
        when(library.checkoutBook(bookName, user)).thenThrow(ItemIsNotAvailableException.invalidBookName());

        bibilioteca.start();

        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showErrorMessage(UserMessages.BOOK_IS_NOT_AVAILABLE);
    }

    @Test
    public void shouldReturnBookIfBookIsCheckedOut() throws Exception, ItemIsNotAvailableException {
        String bookName = "Headfirst Java";

        when(consoleInput.getInteger()).thenReturn(4, 0);
        when(consoleInput.getString()).thenReturn(bookName);
        bibilioteca.start();

        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(library, atLeastOnce()).checkInBook(bookName, user);
        verify(consoleOutput, atLeastOnce()).showMessage(UserMessages.SUCCESSFUL_BOOK_RETURN_MESSAGE);
    }

    @Test
    public void shouldNotifyForUnsuccessfulReturnOfBook() throws Exception, ItemIsNotAvailableException {
        String bookName = "Headfirst Java";

        when(consoleInput.getInteger()).thenReturn(4, 0);
        when(consoleInput.getString()).thenReturn(bookName);
        when(library.checkInBook(bookName, user)).thenThrow(ItemIsNotAvailableException.invalidBookToCheckIn());
        bibilioteca.start();

        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showErrorMessage(UserMessages.UNSUCCESSFUL_BOOK_RETURN_MESSAGE);
    }

    @Test
    public void shouldShowListOfMoviesIfOptionIs5() throws Exception {
        String movieName = "Kal Ho Na Ho";

        when(consoleInput.getInteger()).thenReturn(5, 0);
        when(consoleInput.getString()).thenReturn(movieName);
        when(library.movieList()).thenReturn(movieList);
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(movieList.toString());
    }

    @Test
    public void shouldCheckoutMovieForOption6() throws ItemIsNotAvailableException {
        when(consoleInput.getInteger()).thenReturn(6, 0);
        String movieName = "Kal Ho Na Ho";
        when(consoleInput.getString()).thenReturn(movieName);
        when(library.checkoutMovie(movieName)).thenReturn(new Movie("Kal Ho Na Ho", 2013, "Karan Johar", "9"));
        bibilioteca.start();
        verify(authenticator, times(1)).authenticate(anyString(), anyString());
        verify(consoleOutput, atLeastOnce()).showMessage(WELCOME_MESSAGE);
        verify(consoleOutput, atLeastOnce()).showMessage(SUCCESSFUL_MOVIE_CHECKOUT_MESSAGE);
    }

    @Test
    public void shouldPrintUserInfoForOption7() {
        when(consoleInput.getInteger()).thenReturn(7, 0);
        bibilioteca.start();
        verify(consoleOutput, atLeastOnce()).showMessage(user.toString());
    }
}
